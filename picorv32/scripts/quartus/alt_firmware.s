	.file	"alt_firmware.c"
	.option nopic
	.text
	.align	2
	.globl	put_num
	.type	put_num, @function
put_num:
	and	a0,a0,0xff
	li	a5,268435456
	sb	a0,0(a5)
	ret
	.size	put_num, .-put_num
	.align	2
	.globl	make_y
	.type	make_y, @function
make_y:
	li	a4,8
	li	a5,268435456
	sb	a4,0(a5)
	ret
	.size	make_y, .-make_y
	.align	2
	.globl	write_to_mem
	.type	write_to_mem, @function
write_to_mem:
	add	sp,sp,-32
	sw	s0,24(sp)
	sw	s1,20(sp)
	sw	ra,28(sp)
	li	s0,0
	li	s1,268435456
.L4:
	blt	s0,a1,.L5
	lw	ra,28(sp)
	lw	s0,24(sp)
	lw	s1,20(sp)
	add	sp,sp,32
	jr	ra
.L5:
	lbu	a5,0(a0)
	sw	a1,12(sp)
	sw	a0,8(sp)
	sb	a5,0(s1)
	call	make_y
	lw	a0,8(sp)
	add	s0,s0,1
	lw	a1,12(sp)
	add	a0,a0,4
	j	.L4
	.size	write_to_mem, .-write_to_mem
	.section	.text.startup,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	add	sp,sp,-64
	li	a5,65
	sw	a5,8(sp)
	li	a5,66
	sw	a5,12(sp)
	li	a5,67
	sw	a5,16(sp)
	li	a5,68
	sw	a5,20(sp)
	li	a5,69
	sw	a5,24(sp)
	li	a5,70
	sw	a5,28(sp)
	li	a5,71
	sw	a5,32(sp)
	li	a5,72
	sw	a5,36(sp)
	li	a5,73
	sw	a5,40(sp)
	add	a0,sp,8
	li	a5,74
	li	a1,10
	sw	ra,60(sp)
	sw	a5,44(sp)
	call	write_to_mem
	li	a5,75
	sw	a5,8(sp)
	li	a5,76
	sw	a5,12(sp)
	li	a5,77
	sw	a5,16(sp)
	add	a0,sp,8
	li	a5,78
	li	a1,10
	sw	a5,20(sp)
	call	write_to_mem
	lw	ra,60(sp)
	add	sp,sp,64
	jr	ra
	.size	main, .-main
	.ident	"GCC: (GNU) 6.1.0"
