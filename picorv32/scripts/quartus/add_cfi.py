#!/usr/bin/env python3

# Script to add HCFI instrumentation to an assembly
# file created by gcc -S for the RISC-V architecture.
#
# Author:   Charles Suslowicz
# Email:    cesuslow@vt.edu
#
# ===================================================================


# Major Tasks
#
#   Open and parse file 
#   1. add #include "cfi_ops.S"  
#   2. Find direct calls and add setpc
#   3. Find ret add checkpc
#   4. Find indirect calls and add setpclabel
#   5. Find indirect functions and add checklabel


import sys
import os
import re
import pprint

init_list = ['\n',
    '/* for our custom operations */\n',
    '#include "cfi_ops.S"\n',
    '\n',
    '.section .init\n',
	'.global main\n',
	'\n',
	'/* set stack pointer */\n',
	'lui sp, %hi(16*1024)\n',
	'addi sp, sp, %lo(16*1024)\n',
	'\n',
	'/* call main */\n',
	'jal ra, main\n',
	'\n',
	'/* break */\n',
	'ebreak\n']

def get_assembly_list(filename):
    with open(filename, 'r') as f:
        assembly_list = f.readlines()
    return assembly_list

def get_label_list(asm):
    label_list = []
    for line in asm:
        if re.search('^\S+:$', line):
            label_list.append(line)
    return label_list 

def get_func_list(label_list):
    func_list = []
    for label in label_list:
        if '.' != label[0]:
            func_list.append(label.split(':')[0])
    return func_list

def add_include(asm):
    return init_list + asm

def add_checkpc(asm):
    new_asm = []
    for line in asm:
        if 'ret' in line:
            new_asm.append("\tcfi_checkpc_insn()\n")
        new_asm.append(line)
    return new_asm

def add_setpc(asm, func_list):
    new_asm = []
    for line in asm:
        if 'call' in line:
            for func in func_list:
                if func in line:
                    new_asm.append("\tcfi_setpc_insn()\n")
                    break
            else:
                new_asm.append("\tcfi_setpclabel_insn(LABEL_1)\n")
        new_asm.append(line)
    return new_asm

def add_checklabel(asm):
    for index,line in enumerate(asm):
        if '@function' in line:
            asm.insert(index+2, '\tcfi_checklabel_insn(LABEL_1)\n')
    return asm

def remove_checklabel_from_first_func(asm):
    start = 0
    for index,line in enumerate(asm):
       if '.text.startup' in line:
            start = index
    print('start = ', start) 
    for i in range(start,len(asm)):
        if asm[i] == '\tcfi_checklabel_insn(LABEL_1)\n':
            asm.pop(i)
            return asm
    return asm

def write_new_file(orig_filename, asm):
    name = orig_filename.split('.')[0]
    new_file = name +'-cfi'+ '.S'
    with open(new_file, 'w') as f:
        f.writelines(asm)

def main():
    filename = sys.argv[1]
    orig_asm = get_assembly_list(filename)
    label_list = get_label_list(orig_asm)
    func_list = get_func_list(label_list)

    asm = add_include(orig_asm)
    asm = add_checkpc(asm)
    asm = add_setpc(asm, func_list)
    asm = add_checklabel(asm)
    asm = remove_checklabel_from_first_func(asm)

    #pprint.pprint(asm)
    #pprint.pprint(func_list)

    write_new_file(filename, asm)


if __name__ == "__main__":
    main()

