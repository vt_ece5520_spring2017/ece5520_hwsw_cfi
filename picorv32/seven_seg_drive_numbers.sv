// This module is not registered.  Registered output will rely on the driving connection.
// (C) 2016 Virigina Itech ITSL.  Joseph Sagisi.  jsagisi@vt.edu

//  ---------------------------------------------------------------------------
//  Register Map
//  ---------------------------------------------------------------------------
//  
//  THE FOLLOWING REGISTER MAP CURRENTLY DOES NOT HOLD.  THIS MODULE HAS BEEN MODIFIED
//  TO OUTPUT THE FOLLOWING TO SUPPORT HARDWARE MAPPINGS INSTEAD OF SOFTWARE:
//
//  kEYPRESS:
//		0)	SYSTEM CLOCK (SECONDS)
// 		1)	CONDUIT 1 INPUT
// 		2)	CONDUIT 2 INPUT
// 		3)	CONDUIT 3 INPUT	
//
////////////////////////////////////////////////////////////////////////////////
//
//  The slave interface for the 8ea 7-segment display dirver is broken up into 
//  32-bit registers with the following layout:
//  
//  Register 0 - Base Display Register
//      Bits [31:0] - W - Default value displaye when no buttons are pressed
//                                    
//  Register 1 - Leftmost key button status
//      Bits [31:0] - W - Value is displayed when KEY[3] is pressed
//
//  Register 2 - Leftmost key button status
//      Bits [31:0] - W - Value is displayed when KEY[2] is pressed
//
//  Register 3 - Leftmost key button status
//      Bits [31:0] - W - Value is displayed when KEY[1] is pressed
//
//  R - Readable
//  W - Writeable
//  WC - Clear on Write
//
`timescale 1 ns / 1 ns

module seven_seg_drive_numbers (

    // clock interface
    input           csi_clock_clk,
    input           csi_clock_reset,
    
//	input wire [31:0] in_hex,
	input wire [3:0] in_key_w,
	output reg [6:0] out_7seg_0,
	output reg [6:0] out_7seg_1,
	output reg [6:0] out_7seg_2,
	output reg [6:0] out_7seg_3,
	output reg [6:0] out_7seg_4,
	output reg [6:0] out_7seg_5,
	output reg [6:0] out_7seg_6,
	output reg [6:0] out_7seg_7,

	// // slave interface for Nios II control 
 //    input           avs_s0_write,
 //    input           avs_s0_read,
 //    input   [1:0]   avs_s0_address,
 //    input   [3:0]   avs_s0_byteenable,
 //    input   [31:0]  avs_s0_writedata,
 //    output  [31:0]  avs_s0_readdata,

    // Key 1 Conduit
    input   [31:0]  coe_s1_key1,

    // Key 2 Conduit
    input   [31:0]  coe_s2_key2,

    // Key 3 Conduit
    input   [31:0]  coe_s3_key3,

    // Drive for Contant value
    input 	[31:0]  coe_system_constant
);

//reg [31:0] reg_0, reg_1, reg_2, reg_3;
reg [31:0] out_hex;

task translate;
	input [3:0] rawbin;
	output [6:0] outdrive;
	begin
	outdrive = (rawbin == 4'h0) ? 7'b1000000 : // 0
		  (rawbin == 4'h1) ? 7'b1111001 : // 1
		  (rawbin == 4'h2) ? 7'b0100100 : // 2
		  (rawbin == 4'h3) ? 7'b0110000 : // 3
		  (rawbin == 4'h4) ? 7'b0011001 : // 4
		  (rawbin == 4'h5) ? 7'b0010010 : // 5
		  (rawbin == 4'h6) ? 7'b0000010 : // 6
		  (rawbin == 4'h7) ? 7'b1111000 : // 7
		  (rawbin == 4'h8) ? 7'b0000000 : // 8
		  (rawbin == 4'h9) ? 7'b0011000 : // 9
		  (rawbin == 4'ha) ? 7'b0001000 : // 10
		  (rawbin == 4'hb) ? 7'b0000011 : // 11
		  (rawbin == 4'hc) ? 7'b1000110 : // 12
		  (rawbin == 4'hd) ? 7'b0100001 : // 13
		  (rawbin == 4'he) ? 7'b0000110 : // 14
		  (rawbin == 4'hf) ? 7'b0001110 : // 15
		  					 7'b1111111 ; // null
	end
endtask : translate


// KEY DETECTION
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
if (csi_clock_reset) out_hex <= 32'h88888888;
else
begin
	out_hex <= (in_key_w == 4'b1110) ? 32'h88888888   : // Reset
			 (in_key_w == 4'b1111) ? coe_system_constant : // No button
			 (in_key_w == 4'b0111) ? coe_s1_key1     : // Leftmost
			 (in_key_w == 4'b1011) ? coe_s2_key2     : // Second from left
			 (in_key_w == 4'b1101) ? coe_s3_key3     : // Third from left
			 						 out_hex;		// else no change
end

// RAW TRANSLATION
always @ (out_hex)
begin
translate (out_hex[ 3: 0], out_7seg_0);
translate (out_hex[ 7: 4], out_7seg_1);
translate (out_hex[11: 8], out_7seg_2);
translate (out_hex[15:12], out_7seg_3);
translate (out_hex[19:16], out_7seg_4);
translate (out_hex[23:20], out_7seg_5);
translate (out_hex[27:24], out_7seg_6);
translate (out_hex[31:28], out_7seg_7);
end


// READ FROM REGISTERS
 // assign avs_s0_readdata = (avs_s0_address == 2'h0) ? reg_0 :
 // 					 	  (avs_s0_address == 2'h1) ? reg_1 :
 // 						  (avs_s0_address == 2'h2) ? reg_2 :
 // 						  (avs_s0_address == 2'h3) ? reg_3 :
	// 					 							 32'h0;

// always @ (posedge csi_clock_clk or posedge csi_clock_reset)
// begin
//     if(csi_clock_reset)
//     begin
//         reg_0  <= 32'hZZZZZZZZ;
//         reg_1  <= 32'hZZZZZZZZ;
//         reg_2  <= 32'hZZZZZZZZ;
//         reg_3  <= 32'hZZZZZZZZ;
//     end
//     else if (avs_s0_write)				// Write value to corresponding display latch
//     begin
//         case(avs_s0_address)
//             //2'h0: reg_0  <= avs_s0_writedata;
//             2'h1: reg_1  <= avs_s0_writedata;
//             2'h2: reg_2  <= avs_s0_writedata;
//             2'h3: reg_3  <= avs_s0_writedata;
//             default: ;
//         endcase
//     end 								// If constant input driver is attached, use to override reg_0
//     reg_0  <= coe_system_constant_high ;
// end

endmodule