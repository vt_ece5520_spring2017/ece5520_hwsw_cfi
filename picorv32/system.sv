`timescale 1 ns / 1 ps

// README:
// In order to use FAST_MEM register-based memory, place PicoRV32 generated *.hex file into modelsim directory. 
// In order to use Alter On-Chip RAM IP, modiy the ram_64kb_sp.v file to include the directory of the Intel Format *.hex file.  You may need to use the Nios2 command line tool: nios2-elf-objcopy -O ihex ./{source}.elf ./{dest}.hex  Place this file in the ModelSim directory as well.


/////////////////////////////////////////////////////////
// synthesis translate_off
/*During Simulation, these are autmactically not defined and will connect Altera RAM IP and use sythnesis logic */

//`define FASTMEM // if not defined, Altera ON-Chip RAM will be connected
`define SIMULATE // Do not comment this out.  This is automatically removed during synthesis.
`define MEM_HEX_LOCATION "./alt_firmware-cfi.hex" // used only for Fast MEM.  Place in ModelSim folder.
// synthesis translate_on
///////////////////////////////////////////////////////////
`define MEM_SIZE_DEFINE 4096

/*
I/O

KEY  [0] - RESET
LEDG [0] - Trap
LEDG [1] - Reset
LEDR [0] - CFI Exception
LEDR [1] - Shadow Stack Exception
*/
module system (
	input            CLOCK_50,
	input      [3:0]  KEY,

	output		     [6:0]		HEX0,			//See Pin Planner for output labels
	output		     [6:0]		HEX1,
	output		     [6:0]		HEX2,
	output		     [6:0]		HEX3,
	output		     [6:0]		HEX4,
	output		     [6:0]		HEX5,
	output		     [6:0]		HEX6,
	output		     [6:0]		HEX7,
	output 			 [7:0]		LEDG,			// Active HIGH
	output 			 [17:0]     LEDR

	//output           trap,

);
	parameter FAST_MEMORY = 1;	// set this to 0 for better timing but less performance/MHz
	parameter MEM_SIZE = `MEM_SIZE_DEFINE;	// 4096 32bit words = 16kB memory
//-----------------------
// Top

	wire clk;
	wire resetn;

//-----------------------
// Native PicoRV interface
	wire trap;
	wire valid;
	wire instr;
	wire ready;
	wire [31:0] addr;
	wire [31:0] wdata;
	wire [3:0] wstrb;
	wire [31:0] rdata;
	wire la_read;
	wire la_write;
	wire [31:0] la_addr;
	wire [31:0] la_wdata;
	wire [3:0] la_wstrb;

// Added exeption flags
	wire lifo_exception, cfi_exception;

//-----------------------
// Altera Avalon MM Interface
	wire avln_rden, avln_wren, avln_waitrq, avln_clk, avln_reset;
	wire [3:0] avln_be;
	wire [31:0] avln_data, avln_q, avln_addr;

//-----------------------
// Simulation Memory Interface
	wire mem_valid;
	wire mem_instr;
	reg mem_ready;
	wire [31:0] mem_addr;
	wire [31:0] mem_wdata;
	wire [3:0] mem_wstrb;
	reg [31:0] mem_rdata;
	wire mem_la_read;
	wire mem_la_write;
	wire [31:0] mem_la_addr;
	wire [31:0] mem_la_wdata;
	wire [3:0] mem_la_wstrb;


`ifdef SIMULATE
reg clk_reg, resetn_reg;

always begin 
#5
	clk_reg <= ~ clk_reg;
end

initial begin 
#0
	clk_reg <= 0;
	resetn_reg <= 0;
#10
    resetn_reg <= 1;
end

assign clk = clk_reg;
assign resetn = resetn_reg;
`else 

//////////////////////////////
// Synthesis
//////////////////////////////
//----------------------------
// DE2-115 I/O

	assign resetn = KEY[0];
	assign LEDG[1] = !resetn; // durring reset
	assign clk = CLOCK_50;
	assign LEDG[0] = trap; // system is running
	assign LEDR[1] = lifo_exception;
	assign LEDR[0] = cfi_exception;
`endif

//////////////////////////////
// MEMORY MODULES
//////////////////////////////

`ifdef FASTMEM
//----------------------------
// Original Fast Mem 

	    //reg [31:0] mem_rdata;
	    reg [7:0] out_byte;
	    reg out_byte_en;

		reg [31:0] memory [0:MEM_SIZE-1];
		initial begin
			$display("loading memory size %u", MEM_SIZE);
			$readmemh(`MEM_HEX_LOCATION, memory);
			$display("memory loaded");
		end

		reg [31:0] m_read_data;
		reg m_read_en;

		generate if (FAST_MEMORY) begin
			always @(posedge clk) begin
				mem_ready <= 1;
				out_byte_en <= 0;
				mem_rdata <= memory[mem_la_addr >> 2];
				if (mem_la_write && (mem_la_addr >> 2) < MEM_SIZE) begin
					if (mem_la_wstrb[0]) memory[mem_la_addr >> 2][ 7: 0] <= mem_la_wdata[ 7: 0];
					if (mem_la_wstrb[1]) memory[mem_la_addr >> 2][15: 8] <= mem_la_wdata[15: 8];
					if (mem_la_wstrb[2]) memory[mem_la_addr >> 2][23:16] <= mem_la_wdata[23:16];
					if (mem_la_wstrb[3]) memory[mem_la_addr >> 2][31:24] <= mem_la_wdata[31:24];
				end
				else
				if (mem_la_write && mem_la_addr == 32'h1000_0000) begin
					out_byte_en <= 1;
					out_byte <= mem_la_wdata;
				end
			end
		end else begin
			reg [31:0] mem_rdata;
			reg [7:0] out_byte;
	    	reg out_byte_en;
			always @(posedge clk) begin
				m_read_en <= 0;
				mem_ready <= mem_valid && !mem_ready && m_read_en;

				m_read_data <= memory[mem_addr >> 2];
				mem_rdata <= m_read_data;

				out_byte_en <= 0;

				(* parallel_case *)
				case (1)
					mem_valid && !mem_ready && !mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
						m_read_en <= 1;
					end
					mem_valid && !mem_ready && |mem_wstrb && (mem_addr >> 2) < MEM_SIZE: begin
						if (mem_wstrb[0]) memory[mem_addr >> 2][ 7: 0] <= mem_wdata[ 7: 0];
						if (mem_wstrb[1]) memory[mem_addr >> 2][15: 8] <= mem_wdata[15: 8];
						if (mem_wstrb[2]) memory[mem_addr >> 2][23:16] <= mem_wdata[23:16];
						if (mem_wstrb[3]) memory[mem_addr >> 2][31:24] <= mem_wdata[31:24];
						mem_ready <= 1;
					end
					mem_valid && !mem_ready && |mem_wstrb && mem_addr == 32'h1000_0000: begin
						out_byte_en <= 1;
						out_byte <= mem_wdata;
						mem_ready <= 1;
					end
				endcase
			end
		end 
        endgenerate

//----------------------------
// Connect PicoRV to FastGen Memory

assign mem_valid = valid;
assign mem_instr = instr;
//assign mem_ready = ready;
assign mem_addr  = addr;
assign mem_wdata = wdata;
assign mem_wstrb = wstrb;
//assign mem_rdata = mem_rdata;
assign mem_la_read = la_read;
assign mem_la_write = la_write;
assign mem_la_addr = la_addr;
assign mem_la_wdata = la_wdata;
assign mem_la_wstrb = la_wstrb;

assign ready = mem_ready;
assign rdata = mem_rdata;

`else 

//------------------------------------
// Connect Altera On Chip RAM
assign avln_reset = !resetn;
assign avln_clk = clk;

// Read
assign avln_rden = la_read;//(!(|wstrb)); // all zero and mem_ready
`ifdef SIMULATE
assign avln_addr = {la_addr[31:4],2'b0,la_addr[3:2]}; // this converts 16-word addressing into 4-word addressing
`else 
assign avln_addr = la_addr>>2; // the first two bits are for byte addressing
`endif 

//assign avln_data = la_wdata; //wdata; // FLIP ENDIANNESS
assign avln_data = {la_wdata[0 +: 8], la_wdata[8 +: 8], la_wdata[16 +: 8], la_wdata[24 +: 8]}; //wdata;
//assign rdata = avln_q;
assign rdata = {avln_q[0 +: 8], avln_q[8 +: 8], avln_q[16 +: 8], avln_q[24 +: 8]};
assign ready = !avln_waitrq; // DEBUG.  Setting this to constantly assert 1
// Write
assign avln_wren = la_write;// (|wstrb) & ready; // any value 1
//assign avln_be = la_wstrb;// wstrb;
assign avln_be = {la_wstrb[3], la_wstrb[2], la_wstrb[1], la_wstrb[0]};


//----------------------------
// Generate Memory

	//generate if (USE_M9K_RAM) begin
	initial begin 
					$display("attaching physical RAM");
	end

	ram_64kb_sp #(.depth(4096), .read_latency(2)) ram64kb (
		.clock(avln_clk),
		.aclr(avln_reset), // DEBUG: THIS MIGHT NEED TO BE REVERSE (PLEASE CHECK)

		.address(avln_addr),
		.byteena(avln_be),
		.data(avln_data),
		.rden(avln_rden),
		.wren(avln_wren),
		.q(avln_q),
		.waitrq_a(avln_waitrq)
		);

	initial begin 
			$display("attaching physical RAM complete");
	end

`endif




	picorv32 picorv32_core (
		.clk         (clk         ),
		.resetn      (resetn      ),
		.lifo_exception   (lifo_exception),
		.cfi_exception    (cfi_exception),
		.trap        (     trap),
		.mem_valid   (valid   ),
		.mem_instr   (instr   ),
		.mem_ready   (ready   ),
		.mem_addr    (addr    ),
		.mem_wdata   (wdata   ),
		.mem_wstrb   (wstrb   ),
		.mem_rdata   (rdata   ),
		.mem_la_read (la_read ),
		.mem_la_write(la_write),
		.mem_la_addr (la_addr ),
		.mem_la_wdata(la_wdata),
		.mem_la_wstrb(la_wstrb)
	);

	    seven_seg_drive_numbers sevenseg (
		.csi_clock_clk(clk),
 		.csi_clock_reset(resetn),
    
		.in_key_w(KEY[3:0]),
		.out_7seg_0(HEX0[6:0]),
		.out_7seg_1(HEX1[6:0]),
		.out_7seg_2(HEX2[6:0]),
		.out_7seg_3(HEX3[6:0]),
		.out_7seg_4(HEX4[6:0]),
		.out_7seg_5(HEX5[6:0]),
		.out_7seg_6(HEX6[6:0]),
		.out_7seg_7(HEX7[6:0]),

    	// Display Inputs
		.coe_s1_key1(la_wdata),
		.coe_s2_key2(rdata),
   		.coe_s3_key3(),
		.coe_system_constant(la_read)
);


endmodule
