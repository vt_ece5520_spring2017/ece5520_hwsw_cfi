#define ARR_LENGTH 10


void put_num(int n)
{
	*(volatile char*)0x10000000 = n;
}

void make_y(void)
{
    int i = 0;
    int y = 1;
    for(i; i<3; i++) {
       y = y+y;
    } 
    put_num(y);
}

void write_to_mem(int *numbers, int len) 
{
    int i;
    
    for(i=0; i<len; i++) {
        put_num(numbers[i]);
        make_y();
    }

} 

void main(void)
{
    int numbers[ARR_LENGTH] = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
    
    write_to_mem(numbers, ARR_LENGTH); 

    numbers[0] = 75;
    numbers[1] = 76;
    numbers[2] = 77;
    numbers[3] = 78;

    write_to_mem(numbers, ARR_LENGTH); 
   
}
