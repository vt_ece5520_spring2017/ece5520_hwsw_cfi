create_clock -period 20.00 [get_ports CLOCK_50]


set_false_path					\
	-from	*					\
	-to		[ get_ports {		\
				HEX*    		\
				LED* 			\
			} 					\
			]

set_false_path				\
	-from	[ get_ports {	\
				SW*	       	\
				KEY*	    \
			} ]				\
	-to 	*
