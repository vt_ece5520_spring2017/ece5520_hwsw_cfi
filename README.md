# README #

This repository contains the Verilog and hex files requisite to examine the current benchmarks.  It also contains SRAM Object File (.sof) for quick demonstration on a DE2-115 evaluation board.  Simulations are instruction-accurate and correctly reflect the synthesized files.  Internal to each instruction period (there are up to 8 CPU states per instruction), simulation and synthesis may differ slightly, but the signals converge at the end of instruction in the same number of cycles.

### Quick Demo ###
* SOF Files are uploaded to ./quick_demo_sofs for quick demonstration. SignalTap is required to view signals.
* The Quartus Prime project file is located at ./picorv32/scripts/quartus/synth_system_build/synth_system.qpf
* A few limited screenshots of operation are available at ./images

### How to run simulations ###
* The hex files to be used in memory initialization must be specified at the top of the modified Altera On-Chip RAM IP at ./picorv32/ram_64kb_sp.sv (first memory file definition)
* These files are specified relative to the ModelSim project file.
* Please note that simulations require a binary hex file different from the Intel Hex file format.  These are generated from the PicoRV32/RISC-V toolchain, and are instrumented by the Makefile located at ./picorv32/scripts/quartus/Makefile
* The ModelSim project file is available at ./modelsimfiles/cfi.mpf, and marco defining all interfaces and a collection of useful signals is located at ./modelsimfiles/cfi_signals.do.  However, please note that ModelSim uses direct rather than relative filenames, so a simple search and replace may be necessary to reuse these files.
* Note that ModelSim reads *.hex files in a word-addressable manner, which is different from the requisite byte-addressable manner.  This is not a problem since address translation is automatically generated to adapt for this, but only 1/4 of the memory will be available in simulation using ModelSim

### How to synthesize ###
* The hex files to be used in memory initialization must be specified at the top of the modified Altera On-Chip RAM IP at ./picorv32/ram_64kb_sp.sv (second memory file definition)
* These files are relative to the Quartus project file at ./picorv32/scripts/quartus/synth_system_build
* This is an Intel Hex file format and may be generated from the *.elf file with the command nios2-elf-objcopy -O ihex ./{input file}.elf ./{output file}.hex
* Quartus must be set to read memory initialization files in byte-addressable mode rather than the default word-addressable.  This can be set in Assignments>Settings>Compilation Process Settings> More Settings> Reading or writing Hexadecimal(.hex) File in byte addresable mode On. 
* In this repository, Intel format hex files are appended by *_i.hex

### How to compile new files ###
* For full compilation of binaries from *.c files, the RISC-V and PicoRV32 toolchains but downloaded and installed.  Please see https://github.com/cliffordwolf/picorv32 for full instructions.

### DE2-115 Notes ###
* SW[0] is the reset key
* LEDG[0] is busy
* LEDG[1] is reset
* LEDR[0] is CFI exception detected
* LEDR[1] is Shadow Stack exception detected

### General ###
* Fast Memory (all registers) may also be used.  The hex file is specified within the toplevel file ./picorv32/system.sv
* All Verilog files may be found at ./picorv32
* All hex files may be found at ./modelsimfiles
* Quick run SOF files are located at ./picorv32
* The SignalTap file is located at ./picorv32/scripts/quartus/synth_system_build/stp1.stp
* The folder structure is a bit strange but preserved in order to allow for some of the original scripts to work

### Verilog Files ###
* system.sv - Toplevel file
* picorv32.v - PicoRV32 core
* picorv32_pcpi_cfi.v - CFI module with Pico Co-Processor Interface
* ram_64kb_sp.v - Altera Single Port On-Chip RAM IP (parameterized and with backpressure added)
* seven_seg_drive_numbers.sv - Supporting Hex output driver (not used in current configurations)