/***************************************************************
 * picorv32_pcpi_cfi
 ***************************************************************/
//    instruction:
//    31    25 | 24    20 | 19    15 | 14       12 | 11         7 | 6         0
//    imm[6:0] | rs2[4:0] | rs1[4:0] | funct3[2:0] | ret_reg[4:0] | opcode[6:0]
//
//    opcode  - PCIPI instruction identifier (7'b0101011)
//    ret_reg - XX
//    funct3  - CFI instruction
//    rs1 - Return Address Register (specified as 5'b00001 in ASM)
//    rs2 - PC counter hardwired    (specified as 5'bxxxxx in ASM)
//    imm - label

`define PCIPI_CFI_INSTR_QUAL 7'b0101011

module picorv32_pcpi_cfi (
	input clk, resetn,

	output wire 		lifo_exception,
	output reg 			cfi_exception,

	input             	pcpi_valid,  // i    	    write in instruction into pcpi	
	input      [31:0] 	pcpi_insn,   // i[31:0]	pcpi_insn <= WITH_PCPI ? mem_rdata_q : 'bx; (siphon off the memory read data line)
	input      [31:0] 	pcpi_rs1,    // i[31:0]  // input register as specified by decoded instr [decoded_rs1 <= mem_rdata_latched[24:20];]
	input      [31:0] 	pcpi_rs2,    // i[31:0]  // input register as specified by decoded instr [decoded_rs1 <= mem_rdata_latched[19:15];]
											   // rs1 is set as the PC
	output wire        	pcpi_wr,     // o 	   // output is ready to be read
	output wire [31:0] 	pcpi_rd,     // o[31:0]  // output data
	output reg         	pcpi_wait,   // o 	   // perhaps some kind of backpressure saying it's not yet ready?
	output wire        	pcpi_ready   // o 	   // output is valide
);

localparam [2:0] SETPC      = 3'd0; // Set PC
localparam [2:0] SETPC_WLBL = 3'd1; // Set PC and Label
localparam [2:0] CHK_PC		= 3'd2; // Check PC
localparam [2:0] CHK_LBL 	= 3'd3; // Check Label
localparam CHK_LBL_YES 	= 1'b1;
localparam CHK_LBL_NO   = 1'b0;


//-----------------------------------
// Inteface
assign pcpi_wr	= 1'b1;
assign pcpi_rd  = 32'b0;
assign pcpi_ready = pcpi_wait;// 1'b1;

//-----------------------------------
// LIFO Queue
reg pop, push;
wire [31:0] current_pc, lifo_out_word;

lifo_queue  #( .depth(32),
			   .width(32) ) lifo_queue (
	.clk	   (clk),
	.resetn	   (resetn),
	.pop       (pop),
	.push	   (push),
	.lifo_in   (current_pc + 32'd8),
	.lifo_out  (lifo_out_word),
	.exception (lifo_exception)
	);

//-----------------------------------
// CFI Control
wire pcpi_cfi_cycle /*synthesis keep*/;
wire start;
wire instr_any_cfi;
reg  pcpi_wait_q;

wire [2:0] pcpi_decode = pcpi_insn[14:12];
wire [6:0 ]pcpi_imm    = pcpi_insn[31 -: 7];


assign pcpi_cfi_cycle = start & instr_any_cfi;
assign start = pcpi_wait && !pcpi_wait_q;


//-----------------------------------
// CFI 
wire [31:0] return_addr = pcpi_rs1;
assign current_pc = pcpi_rs2;
reg [7:0] label_reg;
reg f_set_pc;
reg instr_setpc, instr_setpc_lbl, instr_ck_lbl, instr_ck_pc /*synthesis preserve*/;
assign instr_any_cfi = |{instr_setpc, instr_setpc_lbl, instr_ck_lbl, instr_ck_pc};

initial begin 
	$display("Generated HWSW_CFI Module");
end

always @(posedge clk) begin
	instr_setpc <= 0;
	instr_setpc_lbl <= 0;
	instr_ck_lbl <= 0;
	instr_ck_pc <= 0;

	if (resetn && pcpi_valid && pcpi_insn[6:0] == `PCIPI_CFI_INSTR_QUAL) begin
		case (pcpi_insn[14:12])
			SETPC:      instr_setpc <= 1;
			SETPC_WLBL: instr_setpc_lbl <= 1;
			CHK_PC:     instr_ck_lbl <= 1;
			CHK_LBL:    instr_ck_pc <= 1;
		endcase
	end

	pcpi_wait <= instr_any_cfi;
	pcpi_wait_q <= pcpi_wait;
end

reg [31:0] cfi_cycle_count;

always @(posedge clk or negedge resetn) begin
	if (!resetn)
	begin 
		cfi_exception   <= 1'b0;
		push <= 1'b0;
		pop <= 1'b0;
		label_reg <= '{default:0};
		cfi_cycle_count <= '{default:0};
	end
	else
	begin 
		// test that this behaves well with multiple constant drivers
		//push <= 1'b0;
		//pop  <= 1'b0;

		if (pcpi_cfi_cycle)
		begin
		cfi_cycle_count <= cfi_cycle_count + 1;
			$display("cfi_128: execute cfi_instruction");
			case (pcpi_decode)
				SETPC: // 0
				begin 
					label_reg <= {CHK_LBL_NO, pcpi_imm};
					push <= 1'b1;
					$display("cfi_134: set_pc.  label_reg should change.  also push SS");
				end

				SETPC_WLBL:  // 1
				begin 
					label_reg <= {CHK_LBL_YES, pcpi_imm};
					push <= 1'b1;
					$display("cfi_141: set_pc_wlbl.  label_reg should change.  also push to SS");


				end

				CHK_PC: // 2
				begin 
					if(lifo_out_word == return_addr)
						pop <= 1'b1;
					else
						cfi_exception <= 1'b1;
					$display("cfi_152: chk_pc.  check label_reg.  no values should change.  aslo pop SS");

				end

				CHK_LBL: // 3
				begin 
					// Check label only if SETPC_WLBL is last.  Ignore if just SETPC
					if ((label_reg[7] == CHK_LBL_YES) & (label_reg[6:0] != pcpi_imm))
						cfi_exception <= 1'b1;
					$display("cfi_161: chk_lbl.  check label only if valid set. no values should change");
				end
				default : ;
			endcase			
		end
		else
		begin 
			push <= 1'b0;
			pop  <= 1'b0;
		end
	end
end

endmodule


module lifo_queue   #( parameter depth,
					   parameter width )
	(
	input  wire clk,
	input  wire resetn, 
	input  wire push,
	input  wire pop,	
	input  wire [width - 1 : 0] lifo_in,
	output wire [width - 1 : 0] lifo_out,
	output reg exception
	);

reg [depth - 1: 0] stack [width - 1 :0];
reg [$clog2(depth) - 1: 0] count;

assign lifo_out = (count == 0) ? 0 : stack[count - 1];

always @(posedge clk or negedge resetn) begin
	if (!resetn) begin
		stack   <= '{default:0};
		count 	<= '{default:0};
		exception <= 1'b0;
	end
	else
	begin 
		if(push)
		begin
			if(count==depth)
				exception <= 1'b1;
			stack[count] <= lifo_in;
			count 	<= count + 1;
		end
		if(pop)
		begin
			if (count==0)
				exception <= 1'b1;
			count <= count - 1;
		end
	end
end
endmodule