onerror {resume}
quietly virtual signal -install /system { /system/mem_wdata[7:0]} PUTS
quietly virtual signal -install /system { /system/wdata[7:0]} PUTS001
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_addr
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_be
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_data
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_q
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_rden
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_waitrq
add wave -noupdate -group {Avalon On-Chip Ram} -group Interface -radix hexadecimal /system/avln_wren
add wave -noupdate -group {Toplevel Module} -group Parameters /system/FAST_MEMORY
add wave -noupdate -group {Toplevel Module} -group Parameters /system/MEM_SIZE
add wave -noupdate -group {Toplevel Module} -group Signals /system/resetn_reg
add wave -noupdate -group {Toplevel Module} -group Signals /system/clk_reg
add wave -noupdate -group {Toplevel Module} -expand -group Clock /system/resetn
add wave -noupdate -group {Toplevel Module} -expand -group Clock /system/clk
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/lifo_exception
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/cfi_exception
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/trap
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/valid
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/instr
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/ready
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/addr
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/wdata
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/wstrb
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/rdata
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/la_read
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/la_write
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/la_addr
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/la_wdata
add wave -noupdate -group {Toplevel Module} -group {PicoRV Interface} /system/la_wstrb
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_valid
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_instr
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_ready
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_la_wdata
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_la_wstrb
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_rdata
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_la_read
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_la_write
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_la_addr
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_wdata
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Interface -radix hexadecimal /system/mem_wstrb
add wave -noupdate -group {Toplevel Module} -group {Fast Mem Group} -expand -group Signals -radix hexadecimal /system/mem_addr
add wave -noupdate -expand -group {PicoRV32 Module} -radix ascii /system/PUTS001
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/clk
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/resetn
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/trap
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/lifo_exception
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/cfi_exception
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_valid
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_instr
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_ready
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_addr
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_wdata
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_wstrb
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_rdata
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_la_read
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_la_write
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_la_addr
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_la_wdata
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Interface -radix hexadecimal /system/picorv32_core/mem_la_wstrb
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/cpu_state
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix unsigned /system/picorv32_core/count_instr
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal -childformat {{{/system/picorv32_core/current_pc[31]} -radix hexadecimal} {{/system/picorv32_core/current_pc[30]} -radix hexadecimal} {{/system/picorv32_core/current_pc[29]} -radix hexadecimal} {{/system/picorv32_core/current_pc[28]} -radix hexadecimal} {{/system/picorv32_core/current_pc[27]} -radix hexadecimal} {{/system/picorv32_core/current_pc[26]} -radix hexadecimal} {{/system/picorv32_core/current_pc[25]} -radix hexadecimal} {{/system/picorv32_core/current_pc[24]} -radix hexadecimal} {{/system/picorv32_core/current_pc[23]} -radix hexadecimal} {{/system/picorv32_core/current_pc[22]} -radix hexadecimal} {{/system/picorv32_core/current_pc[21]} -radix hexadecimal} {{/system/picorv32_core/current_pc[20]} -radix hexadecimal} {{/system/picorv32_core/current_pc[19]} -radix hexadecimal} {{/system/picorv32_core/current_pc[18]} -radix hexadecimal} {{/system/picorv32_core/current_pc[17]} -radix hexadecimal} {{/system/picorv32_core/current_pc[16]} -radix hexadecimal} {{/system/picorv32_core/current_pc[15]} -radix hexadecimal} {{/system/picorv32_core/current_pc[14]} -radix hexadecimal} {{/system/picorv32_core/current_pc[13]} -radix hexadecimal} {{/system/picorv32_core/current_pc[12]} -radix hexadecimal} {{/system/picorv32_core/current_pc[11]} -radix hexadecimal} {{/system/picorv32_core/current_pc[10]} -radix hexadecimal} {{/system/picorv32_core/current_pc[9]} -radix hexadecimal} {{/system/picorv32_core/current_pc[8]} -radix hexadecimal} {{/system/picorv32_core/current_pc[7]} -radix hexadecimal} {{/system/picorv32_core/current_pc[6]} -radix hexadecimal} {{/system/picorv32_core/current_pc[5]} -radix hexadecimal} {{/system/picorv32_core/current_pc[4]} -radix hexadecimal} {{/system/picorv32_core/current_pc[3]} -radix hexadecimal} {{/system/picorv32_core/current_pc[2]} -radix hexadecimal} {{/system/picorv32_core/current_pc[1]} -radix hexadecimal} {{/system/picorv32_core/current_pc[0]} -radix hexadecimal}} -subitemconfig {{/system/picorv32_core/current_pc[31]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[30]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[29]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[28]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[27]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[26]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[25]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[24]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[23]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[22]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[21]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[20]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[19]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[18]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[17]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[16]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[15]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[14]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[13]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[12]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[11]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[10]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[9]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[8]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[7]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[6]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[5]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[4]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[3]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[2]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[1]} {-height 15 -radix hexadecimal} {/system/picorv32_core/current_pc[0]} {-height 15 -radix hexadecimal}} /system/picorv32_core/current_pc
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/reg_next_pc
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/reg_pc
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/cpuregs
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/cpuregs_rs1
add wave -noupdate -expand -group {PicoRV32 Module} -expand -group Signals -radix hexadecimal /system/picorv32_core/cpuregs_rs2
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {214 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 242
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {57959 ns} {58413 ns}
