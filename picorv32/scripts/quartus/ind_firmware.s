	.file	"ind_firmware.c"
	.option nopic
	.text
	.align	2
	.globl	addnum
	.type	addnum, @function
addnum:
	add	a0,a0,a1
	ret
	.size	addnum, .-addnum
	.align	2
	.globl	subnum
	.type	subnum, @function
subnum:
	sub	a0,a0,a1
	ret
	.size	subnum, .-subnum
	.align	2
	.globl	add2num
	.type	add2num, @function
add2num:
	add	a0,a1,a0
	sll	a0,a0,1
	ret
	.size	add2num, .-add2num
	.align	2
	.globl	sub2num
	.type	sub2num, @function
sub2num:
	sub	a0,a0,a1
	sll	a0,a0,1
	ret
	.size	sub2num, .-sub2num
	.align	2
	.globl	put_num
	.type	put_num, @function
put_num:
	and	a0,a0,0xff
	li	a5,268435456
	sb	a0,0(a5)
	ret
	.size	put_num, .-put_num
	.align	2
	.globl	make_y
	.type	make_y, @function
make_y:
	li	a4,8
	li	a5,268435456
	sb	a4,0(a5)
	ret
	.size	make_y, .-make_y
	.align	2
	.globl	write_to_mem
	.type	write_to_mem, @function
write_to_mem:
	add	sp,sp,-32
	sw	s0,24(sp)
	sw	s1,20(sp)
	sw	ra,28(sp)
	li	s0,0
	li	s1,268435456
.L8:
	blt	s0,a1,.L9
	lw	ra,28(sp)
	lw	s0,24(sp)
	lw	s1,20(sp)
	add	sp,sp,32
	jr	ra
.L9:
	lbu	a5,0(a0)
	sw	a1,12(sp)
	sw	a0,8(sp)
	sb	a5,0(s1)
	call	make_y
	lw	a0,8(sp)
	add	s0,s0,1
	lw	a1,12(sp)
	add	a0,a0,4
	j	.L8
	.size	write_to_mem, .-write_to_mem
	.section	.text.startup,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	add	sp,sp,-96
	li	a5,65
	sw	a5,24(sp)
	li	a5,66
	sw	a5,28(sp)
	li	a5,67
	sw	a5,32(sp)
	li	a5,68
	sw	a5,36(sp)
	li	a5,69
	sw	a5,40(sp)
	li	a5,70
	sw	a5,44(sp)
	li	a5,71
	sw	a5,48(sp)
	li	a5,72
	sw	a5,52(sp)
	li	a5,73
	sw	a5,56(sp)
	li	a5,74
	sw	a5,60(sp)
	lui	a5,%hi(addnum)
	add	a5,a5,%lo(addnum)
	sw	a5,8(sp)
	lui	a5,%hi(subnum)
	add	a5,a5,%lo(subnum)
	sw	a5,12(sp)
	lui	a5,%hi(add2num)
	add	a5,a5,%lo(add2num)
	sw	a5,16(sp)
	lui	a5,%hi(sub2num)
	add	a5,a5,%lo(sub2num)
	li	a1,10
	add	a0,sp,24
	sw	s0,88(sp)
	sw	s1,84(sp)
	sw	s2,80(sp)
	sw	s3,76(sp)
	sw	ra,92(sp)
	sw	a5,20(sp)
	li	s0,0
	call	write_to_mem
	li	s3,3
	li	s2,5
	li	s1,16
.L12:
	add	a5,sp,8
	add	a5,a5,s0
	lw	a5,0(a5)
	mv	a1,s3
	mv	a0,s2
	add	s0,s0,4
	jalr	a5
	bne	s0,s1,.L12
	and	a0,a0,0xff
	li	a5,268435456
	sb	a0,0(a5)
	lw	ra,92(sp)
	lw	s0,88(sp)
	lw	s1,84(sp)
	lw	s2,80(sp)
	lw	s3,76(sp)
	add	sp,sp,96
	jr	ra
	.size	main, .-main
	.ident	"GCC: (GNU) 6.1.0"
