#define ARR_LENGTH 10


void put_num(int n)
{
	*(volatile char*)0x10000000 = n;
}

int addnum(int x, int y)
{
    return x+y;
}

int subnum(int x, int y)
{
    return x-y;
}

int add2num(int x, int y)
{
    return x+x+y+y;
}

int sub2num(int x, int y)
{
    return (x+x) - (y+y);
}

void make_y(void)
{
    int i = 0;
    int y = 1;
    for(i; i<3; i++) {
       y = y+y;
    } 
    put_num(y);
}

void write_to_mem(int *numbers, int len) 
{
    int i;
    
    for(i=0; i<len; i++) {
        put_num(numbers[i]);
        make_y();
    }

} 

void main(void)
{
    int numbers[ARR_LENGTH] = { 65, 66, 67, 68, 69, 70, 71, 72, 73, 74 };
    int (*fn)(int,int);
    int (*func_arr[4])(int,int) = { &addnum, &subnum, &add2num, &sub2num };
    int x = 5;
    int y = 3;
    int i = 0;
    int res = 0;
    
    write_to_mem(numbers, ARR_LENGTH);    
    
    for(i; i<4; i++){
        fn = func_arr[i];
        res = fn(x,y);
    }
    
    put_num(res);  

}
