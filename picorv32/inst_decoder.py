#!/usr/bin/env python3

import sys
import os

instruction = sys.argv[1]

bin_str = "{:032b}".format(int(instruction, 16))

print("imm[6:0]   ", bin_str[:-25])
print("rs2[4:0]   ", bin_str[-25:-20])
print("rs1[4:0]   ", bin_str[-20:-15])
print("funct3[2:0]", bin_str[-15:-12])
print("rd[4:0]    ", bin_str[-12:-7])
print("opcode     ", bin_str[-7:])
